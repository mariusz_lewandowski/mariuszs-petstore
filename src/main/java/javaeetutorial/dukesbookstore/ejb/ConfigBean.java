
package javaeetutorial.dukesbookstore.ejb;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 * <p>Singleton bean that initializes the book database for the bookstore
 * example.</p>
 */
@Singleton
@Startup
public class ConfigBean {

    @EJB
    private BookRequestBean request;

    @PostConstruct
    public void createData() {
        //request.createBook("201", "Duke", "","My Early Years: Growing Up on *7",30.75, false, 2005, "What a cool book.", 20);
        request.createBook("201", "mammal", "carnivore", "Cat", 20.00, false, 2013, "A vary nice, one year old cat!", 2);
        //request.createBook("202", "Jeeves", "","Web Servers for Fun and Profit", 40.75, true,2010, "What a cool book.", 20);
        request.createBook("202", "mammal", "omnivore", "Piglet", 49.99, true, 2011, "a pink creature, some prefer over a dog", 12);
        //request.createBook("203", "Masterson", "Webster","Web Components for Web Developers",27.75, false, 2010, "What a cool book.", 20);
        request.createBook("203", "mammal", "herbivore", "Squirrel", 11.19, false, 2014, "you never know when you might need one", 44);
        //request.createBook("205", "Novation", "Kevin","From Oak to Java: The Revolution of a Language",10.75, true, 2008, "What a cool book.", 20);
        request.createBook("205", "mammal", "carnivore", "Dog", 15.00, true, 2013, "good if you do not know what to do with your meal leftovers", 99);
        //request.createBook("206", "Thrilled", "Ben","The Green Project: Programming for Consumer Devices",30.00, true, 2008, "What a cool book.", 20);
        request.createBook("206", "mammal", "omivore", "Monkey", 69.99, true, 2011, "it is much better than Kitten", 2);
        //request.createBook("207", "Coding", "Happy","Java Intermediate Bytecodes", 30.95, true,2010, "What a cool book.", 20);
        request.createBook("207", "mammal", "carnivore", "Kitten", 99.99, true, 1994, "very expensive to maintain, causes lots of problems...", 1);

    }
}
